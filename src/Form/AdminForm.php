<?php
/**
 * admin form
 *
 * 开发公司：未来很美（深圳）科技有限公司 (www.will-nice.com)
 * 开发者：云客 (www.indrupal.com)
 * 微信号（WeChat）：indrupal
 * Email:phpworld@qq.com
 *
 */

namespace Drupal\renew\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Render\Markup;
use Drupal\Core\Datetime\DrupalDateTime;

class AdminForm extends FormBase {

  /**
   * @var \Drupal\Core\Config\Config|null
   */
  protected $config = NULL;

  /**
   * @var bool
   */
  protected $isLogin = FALSE;

  /**
   * @param \Drupal\Core\Config\Config $config
   */
  public function __construct(Config $config) {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory')->getEditable('renew.admin'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yunke_renew_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('submit'),
      '#button_type' => 'primary',
    ];
    if (!$this->isLogin) {
      if ($this->config->get('password')) {
        // show login form
        $form['password'] = [
          '#type'     => 'password',
          '#title'    => $this->t('Password'),
          '#required' => TRUE,
        ];
        if ($passwordHintMsg = \Drupal::config('renew.admin')->get('passwordHintMsg')) {
          $form['passwordHintMsg'] = [
            '#markup' => Markup::create($passwordHintMsg),
          ];
        }
        $form['actions']['submit']['#value'] = $this->t('Login');
        $form['#title'] = $this->t('Login');
        $this->copyright($form);
        return $form;
      }
      else {
        //first time enter the system , set password
        $form['msg'] = [
          '#markup' => $this->t('Welcome, this is your first time to Renew module, Set a password for admin.'),
        ];
        $form['password'] = [
          '#type'     => 'password_confirm',
          '#required' => TRUE,
        ];
        $form['actions']['submit']['#value'] = $this->t('Set admin password');
        $form['#title'] = $this->t('Welcome to Renew module');
        $this->copyright($form);
        return $form;
      }
    }

    //show admin form
    $form['expirationTime'] = [
      '#type'                => 'datetime',
      '#title'               => $this->t('expiration time'),
      '#required'            => TRUE,
      '#description'         => $this->t('After the time, Grace period begin, user will be notified'),
      '#date_date_format'    => 'Y-m-d',
      '#date_date_element'   => 'date',
      '#date_date_callbacks' => [],
      '#date_time_format'    => 'H:i:s',
      '#date_time_element'   => 'time',
      '#date_time_callbacks' => [],
      '#date_year_range'     => '1984:2180',
      '#date_increment'      => 1,
      '#default_value'       => DrupalDateTime::createFromTimestamp($this->config->get('expirationTime')),
    ];

    $form['graceTime'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Grace period'),
      '#description'   => $this->t('in seconds, default 1296000(15 days). During the grace period, System is still running, but messages are displayed'),
      '#field_suffix'  => $this->t('e.g: 86400(1 days), 604800(7 days), 864000(10 days), 2592000(30 days), 7776000(3 months) and so on'),
      '#min'           => 0,
      '#step'          => 1,
      '#default_value' => $this->config->get('graceTime'),
    ];

    $form['graceMsg'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Grace period notice message'),
      '#description'   => $this->t('User will get the message during the grace period, Any token is available, Expiration time:[renew:expiration] System stop time:[renew:grace]'),
      '#default_value' => $this->config->get('graceMsg'),
    ];

    $form['onlyAdminPageNotice'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Grace period message only in admin page'),
      '#description'   => $this->t('During the grace period, only the admin page will be notified, otherwise the whole site will be notified'),
      '#default_value' => $this->config->get('onlyAdminPageNotice'),
    ];
    $form['expirationMsg'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('expiration stop message'),
      '#description'   => $this->t('After grace period, System will stop and the message will be displayed, Any token is available, Expiration time:[renew:expiration] System stop time:[renew:grace]'),
      '#default_value' => $this->config->get('expirationMsg'),
    ];

    $form['enableEmailNotice'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('email notice'),
      '#description'   => $this->t('Whether send email notification to Email address in Site settings'),
      '#default_value' => $this->config->get('enableEmailNotice'),
    ];
    $form['enableUninstall'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('allow uninstall Renew module '),
      '#description'   => $this->t('Whether Renew module can be uninstalled'),
      '#default_value' => $this->config->get('enableUninstall'),
    ];
    $form['passwordSetting'] = [
      '#type'  => 'details',
      '#open'  => FALSE,
      '#title' => $this->t('Password Setting'),
    ];
    $form['passwordSetting']['password'] = [
      '#type'  => 'password_confirm',
      '#title' => $this->t('Change password (The old password is not required)'),
    ];
    $form['passwordSetting']['passwordHintMsg'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Password Hint'),
      '#description'   => $this->t('Help remember passwords, Displayed in Login form'),
      '#default_value' => $this->config->get('passwordHintMsg'),
    ];
    $this->copyright($form);
    return $form;

  }

  protected function copyright(&$form) {
    $form['willnice'] = [
      '#markup' => Markup::create('Power by: <a href="http://www.will-nice.com" target="_blank">未来很美 Will-Nice</a>'),
      '#weight' => 10000,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $savedPassword = $this->config->get('password');
    if (!$this->isLogin && $savedPassword) {
      //validate login password
      $password = $form_state->getValue('password');
      if (empty($password)) {
        $form_state->setError($form['password'], $this->t('Password  is required'));
      }
      $hash = hash("sha256", $this->config->get('installTime') . $password);
      if (!hash_equals($hash, $savedPassword)) {
        $form_state->setError($form['password'], $this->t('Password error'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!$this->isLogin) {
      $savedPassword = $this->config->get('password');
      if (empty($savedPassword)) {
        $password = $form_state->getValue('password');
        $hash = hash("sha256", $this->config->get('installTime') . trim($password));
        $this->config->set('password', $hash);
        $this->config->save();
        $this->isLogin = TRUE;
      }
      else {
        $this->isLogin = TRUE;
      }
      $form_state->setRebuild();
      return;
    }
    $expirationTime = $form_state->getValue($form['expirationTime']['#parents'])->getTimestamp();
    $this->config->set('expirationTime', $expirationTime);
    $this->config->set('graceTime', $form_state->getValue('graceTime'));
    $this->config->set('graceMsg', $form_state->getValue('graceMsg'));
    $this->config->set('onlyAdminPageNotice', $form_state->getValue('onlyAdminPageNotice'));
    $this->config->set('expirationMsg', $form_state->getValue('expirationMsg'));
    $this->config->set('enableEmailNotice', $form_state->getValue('enableEmailNotice'));
    $this->config->set('enableUninstall', $form_state->getValue('enableUninstall'));
    if ($password = $form_state->getValue('password')) {
      $hash = hash("sha256", $this->config->get('installTime') . trim($password));
      $this->config->set('password', $hash);
    }
    $this->config->set('passwordHintMsg', $form_state->getValue('passwordHintMsg'));
    $this->config->save();
    //drupal_flush_all_caches();
    $this->messenger()->addMessage($this->t('Settings saved'));
    $this->isLogin = TRUE;
    $form_state->setRebuild();
  }

}
