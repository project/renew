<?php

/**
 * 开发公司：未来很美（深圳）科技有限公司 (www.will-nice.com)
 * 开发者：云客 (www.indrupal.com)
 * 微信号（WeChat）：indrupal
 * Email:phpworld@qq.com
 *
 */

namespace Drupal\renew\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Renew maintenance subscriber for controller requests.
 */
class MaintainRequestSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * the renew config
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new MaintainRequestSubscriber
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface          $config_factory
   * @param \Drupal\Core\Messenger\MessengerInterface           $messenger
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, TranslationInterface $translation) {
    $this->config = $config_factory->get('renew.admin');
    $this->messenger = $messenger;
    $this->stringTranslation = $translation;
  }

  /**
   * Notify the user that a renewal is required.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   */
  public function onRequest(RequestEvent $event) {
    $request = $event->getRequest();
    $route_match = RouteMatch::createFromRequest($request);
    if ($route_match->getRouteName() == 'renew.admin') {
      return;
    }
    $requestTime = \Drupal::time()->getRequestTime();
    $expirationTime = $this->config->get('expirationTime');
    $graceTime = $this->config->get('graceTime');
    $message = '';
    if ($requestTime <= $expirationTime) {
      return;
    }
    elseif ($requestTime > $expirationTime && $requestTime <= $expirationTime + $graceTime) { //During the grace period
      $message = $this->config->get('graceMsg');
      $message = \Drupal::token()->replace($message);
      if ($this->config->get('onlyAdminPageNotice')) {
        if (\Drupal::service('router.admin_context')->isAdminRoute($route_match->getRouteObject())) {
          $this->messenger->addWarning($message);
        }
      }
      else {
        $this->messenger->addWarning($message);
      }
    }
    else { //It's already over time
      $message = $this->config->get('expirationMsg');
      $message = \Drupal::token()->replace($message);
      if ($request->getRequestFormat() === 'html') {
        $response = new Response($message);
        $event->setResponse($response);
      }
      else {
        $response = new Response($message, 503, ['Content-Type' => 'text/plain']);
        $event->setResponse($response);
      }
    }
    \Drupal::service('page_cache_kill_switch')->trigger();
    if ($this->config->get('enableEmailNotice')) {//send email
      //Email only once a day
      $state = \Drupal::state();
      $preSendTime = $state->get(RENEW_EMAIL_TIME);
      $currentSendTime = date('Y-m-d', $requestTime);
      if (strcmp($preSendTime, $currentSendTime) !== 0) {
        if ($this->sendEmail($message)) {
          $state->set(RENEW_EMAIL_TIME, $currentSendTime);
        }
      }
    }

  }

  /**
   * send email
   *
   * @param $message
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function sendEmail($message = '') {
    if (empty($message)) {
      return TRUE;
    }
    $isSend = FALSE;//If one of they succeeds, it's a success
    $siteConfig = \Drupal::config('system.site');
    $siteName = $siteConfig->get('name');
    $params['subject'] = $siteName . " : " . $this->t('your site need renewal ! risk!');
    $params['body'] = [$siteName . " : " . $message];
    $mailManager = \Drupal::service('plugin.manager.mail');
    $siteMail = $siteConfig->get('mail');
    $langCode = \Drupal::service('language.default')->get()->getId();
    if ($siteMail) {
      $result = $mailManager->mail('renew', 'renew-notification', $siteMail, $langCode, $params);
      if ($result['result']) {
        $isSend = TRUE;
      }
    }
    $adminUser = \Drupal::entityTypeManager()->getStorage('user')->load(1);
    $adminUserMail = $adminUser->getEmail();
    if ($adminUserMail) {
      $langCode = $adminUser->getPreferredLangcode();
      $result = $mailManager->mail('renew', 'renew-notification', $adminUserMail, $langCode, $params);
      if ($result['result']) {
        $isSend = TRUE;
      }
    }
    return $isSend;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onRequest', 31];
    return $events;
  }

}
