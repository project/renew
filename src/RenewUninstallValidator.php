<?php
/**
 * 开发公司：未来很美（深圳）科技有限公司 (www.will-nice.com)
 * 开发者：云客 (www.indrupal.com)
 * 微信号（WeChat）：indrupal
 * Email:phpworld@qq.com
 *
 */

namespace Drupal\renew;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;

/**
 * Ensures that renew modules cannot be uninstalled.
 */
class RenewUninstallValidator implements ModuleUninstallValidatorInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new RenewUninstallValidator.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface          $config_factory
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   */
  public function __construct(ConfigFactoryInterface $config_factory, TranslationInterface $string_translation) {
    $this->config = $config_factory->get('renew.admin');
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];
    if ($module !== 'renew') {
      return $reasons;
    }
    if ($this->config->get('enableUninstall')) {
      return $reasons;
    }
    $reasons[] = $this->t('The @module module is required for system maintenance', ['@module' => $module]);
    return $reasons;
  }

}
